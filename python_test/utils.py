# coding=utf-8
import json
import os
import traceback
import uuid
from datetime import datetime
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.http import HttpResponseRedirect
from django.template import Template, Context, loader
from slugify import slugify


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    now = datetime.now()
    return os.path.join(getattr(settings, 'IMAGE_UPLOAD_TO', 'catalog/'), datetime.strftime(now, '%Y/%m/'), filename)


def get_ics_path(instance, filename):
    return os.path.join(getattr(settings, 'IMAGE_UPLOAD_TO', 'outlook/'), str(instance.id), filename)


def create_slug(model, name):
    slug = slugify(name)
    slug_test = ''
    i = 1
    exists = model.objects.filter(slug=slug).exists()
    while exists:
        slug_test = u'%s-%s' % (slug, i)
        exists = model.objects.filter(slug=slug_test).exists()
        i += i
    if slug_test:
        slug = slug_test
    return slug


def copy_object(modeladmin, request, queryset):
    for pr in queryset:
        pr.pk = None
        pr.id = None
        try:
            pr.slug = create_slug(modeladmin.model, pr.name)
        except:
            pass
        pr.save()
    modeladmin.message_user(request, u'Копирование завершено')
    return HttpResponseRedirect(request.get_full_path())


copy_object.short_description = u'Копирование объекта'

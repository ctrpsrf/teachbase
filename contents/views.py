# vim:fileencoding=utf-8
from django.views.generic import TemplateView, CreateView, ListView, DetailView
from django.http import JsonResponse
from .models import FeedBack, Course, CourseRelation
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from provider_oauth.api_wrapper import TeachBaseClient as TBClient
from profiles.models import User

class FrontView(TemplateView):
    template_name = 'front.html'

    # @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = {}
        return ctx


class CoursesListView(ListView):
    model = Course
    template_name = 'contents/courses.html'

    def get_context_data(self, **kwargs):
        context = super(CoursesListView, self).get_context_data(**kwargs)
        context['relations'] = CourseRelation.objects.filter(user_id = self.request.user.id)
        return context


class FeedBackMixin(object):
    def form_invalid(self, form):
        response = super(FeedBackMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        response = super(FeedBackMixin, self).form_valid(form)
        if self.request.is_ajax():
            data = {
                'success': True,
            }
            return JsonResponse({'message': 'Your feedback is processing', 'tags': 'success'})

        else:
            return response

class FeedBackCreate(FeedBackMixin, CreateView):
    template_name = 'contents/feedback.html'
    model = FeedBack
    success_url = '/'
    fields = ['email', 'message']


class CourseRelateMixin(object):
    def form_invalid(self, form):
        response = super(FeedBackMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        response = super(FeedBackMixin, self).form_valid(form)
        tb = TBClient()
        sessions = tb.get_course_sessions(form.course_id)
        if not len(sessions):
            return JsonResponse({'message': 'Not found sessions for this course!', 'tags': 'error'})
        user = User.objects.get(id = form.user_id)

        data = {
            "email": user.email,
            "user_id": user.out_id
        }

        tb.register_user_course_session(sessions[0]['id'], data = data)         # Предполагается, что мы его запишем на первую сессию в списке
        course_relation = CourseRelation.objects.create(
            user = user,
            course = Course.objects.get(id = form.course_id),
            session_id = sessions[0]['id']
        )
        if self.request.is_ajax():
            data = {
                'success': True,
            }
            return JsonResponse({'message': 'Your register to course!', 'tags': 'success'})

        else:
            return response

class CourseRelateCreate(FeedBackMixin, CreateView):
    template_name = 'contents/course_relation.html'
    model = CourseRelation
    success_url = '/'
    fields = ['course_id', 'user_id']


def update_courses(request):
    tb = TBClient()
    courses = tb.get_courses()
    for item in courses:
        tmp_course = Course(
            tb_id = item['id'],
            name = item['name'],
            owner_id = item['owner_id'],
            content_type = item['content_type'],
            owner_name = item['owner_name'],
            description = item['description']
        )
        tmp_course.save()

    return JsonResponse({'message': 'Courses is update!', 'tags': 'success'})

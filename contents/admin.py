from django.contrib import admin
from .models import FeedBack, Course

class FeedBackAdmin(admin.ModelAdmin):
    list_display = ('email', 'message')


class CourseAdmin(admin.ModelAdmin):
    list_display = ('tb_id', 'name', 'content_type')

admin.site.register(FeedBack, FeedBackAdmin)
admin.site.register(Course, CourseAdmin)

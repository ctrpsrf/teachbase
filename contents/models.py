from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'profiles.User')

class FeedBack(models.Model):
    email = models.EmailField(_('Email address'))
    message = models.TextField(_('Message'))

    def __str__(self):
        return self.email

class Course(models.Model):
    tb_id = models.PositiveIntegerField(_('Course ID'))
    name = models.CharField(_('Course Name'), max_length = 512)
    owner_id = models.PositiveIntegerField(_('Owner ID'))
    content_type = models.PositiveIntegerField()
    owner_name = models.CharField(_('Course developer'), max_length = 256)
    description = models.TextField(_('Description') ,blank = True, null = True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-name']

class CourseRelation(models.Model):
    user_id = models.PositiveIntegerField(_('User ID'))
    # user_id = models.ForeignKey(AUTH_USER_MODEL, on_delete = models.DO_NOTHING)   # Чтобы не тратить время на переделку к ajax
    course_id = models.PositiveIntegerField(_('Course ID'))
    # course_id = models.ForeignKey(Course, on_delete = models.DO_NOTHING)
    session_id = models.PositiveIntegerField(_('Session ID'), blank = True, null = True)

    is_confirm = models.BooleanField(_('Confirmation'), default = False)

    def __str__(self):
        return self.id

    def purchase(self):
        from provider_oauth.api_wrapper import TeachBaseClient as TBClient
        tb_client = TBClient

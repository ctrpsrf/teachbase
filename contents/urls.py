from django.urls import path, re_path
from .views import *

app_name = 'contents'

urlpatterns = [
    path('feedback/', FeedBackCreate.as_view(), name='feedback_create'),
    path('courses/', CoursesListView.as_view(), name='courses'),
    path('courses/update', update_courses, name='courses'),
    path('courses/relation', CourseRelateCreate.as_view(), name='course_relation'),
    ]

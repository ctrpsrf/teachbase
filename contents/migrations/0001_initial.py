# Generated by Django 2.2.9 on 2020-02-11 06:21

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tb_id', models.PositiveIntegerField(verbose_name='Course ID')),
                ('name', models.CharField(max_length=512, verbose_name='Course Name')),
                ('owner_id', models.PositiveIntegerField(verbose_name='Owner ID')),
                ('content_type', models.PositiveIntegerField()),
                ('owner_name', models.CharField(max_length=256, verbose_name='Course developer')),
            ],
        ),
        migrations.CreateModel(
            name='FeedBack',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254, verbose_name='Email address')),
                ('message', models.TextField(verbose_name='Message')),
            ],
        ),
        migrations.CreateModel(
            name='CourseRelation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('session_id', models.PositiveIntegerField(verbose_name='Session ID')),
                ('is_confirm', models.BooleanField(default=False, verbose_name='Confirmation')),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='contents.Course')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]

from django.urls import path, re_path
from .views import *

app_name = 'profiles'

urlpatterns = [
    path('signin/', SignInView.as_view(), name='signin'),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('logout/', LogoutView.as_view(), name='logout'),
    # path('settings', SettingView.as_view(), name='settings'),

    ]

from django.views.generic import FormView, RedirectView, TemplateView
from profiles.forms import AuthenticationForm, UserCreationForm
from provider_oauth.models import Client
from provider_oauth.api_wrapper import TeachBaseClient as TBClient
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import login, logout
from django.utils.decorators import method_decorator

class SignInView(FormView):

    template_name = 'profiles/signin.html'
    form_class = AuthenticationForm
    success_url = 'https://go.teachbase.ru/accounts/oauth?provider=pytest'

    @method_decorator(sensitive_post_parameters('password'))
    @method_decorator(csrf_protect)
    def dispatch(self, request, *args, **kwargs):
        # request.session.set_test_cookie()

        return super(SignInView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())

        return super(SignInView, self).form_valid(form)

    def get_success_url(self):
        client = Client.objects.get(name = 'pytest')
        return self.success_url

class SignUpView(FormView):

    template_name = 'profiles/signup.html'
    form_class = UserCreationForm
    success_url = 'https://go.teachbase.ru/accounts/oauth?provider=pytest'

    def dispatch(self, request, *args, **kwargs):
        return super(SignUpView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.save()
        tb_client = TBClient()
        tb_user = tb_client.create_user(user.email, user.id)
        user.out_id = tb_user[0]['id']
        user.save()
        login(self.request, user)
        return super(SignUpView, self).form_valid(form)

    def get_success_url(self):
        return self.success_url



class LogoutView(RedirectView):
    """
    Provides users the ability to logout
    """
    url = '/profiles/signin/'

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)

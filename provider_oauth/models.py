from django.db import models
from django.conf import settings
from .utils import now, short_token, long_token, get_token_expiry

try:
    from django.utils import timezone
except ImportError:
    timezone = None

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'profiles.User')


class Client(models.Model):
    name = models.CharField(max_length=255, blank=True, unique=True)
    url = models.URLField(help_text="Your application's URL.")
    redirect_uri = models.URLField(help_text="Your application's callback URL", blank=True, null=True)
    client_id = models.CharField(max_length=255, blank=True, null=True)
    client_secret = models.CharField(max_length=255, blank=True, null=True)
    expire = models.DateTimeField(blank=True, null=True)

    def generate_secret(self):
        if now() > self.expire:
            self.expire = get_token_expiry()
            self.client_secret = long_token()
            self.save()

    def __str__(self):
        return self.name


class AccessTokenManager(models.Manager):
    def get_token(self, token):
        # return self.get(token=token)
        return self.get(token=token, expires__gt=now())


class AccessTokenModel(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE)
    token = models.CharField(max_length=255, default=long_token, db_index=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    expires = models.DateTimeField()
    objects = AccessTokenManager()

    def __str__(self):
        return self.token

    def save(self, *args, **kwargs):
        if not self.expires:
            self.expires = self.client.get_default_token_expiry()
        super(AccessTokenModel, self).save(*args, **kwargs)

from .views import AccessToken, SuccessAuthorisation
from django.conf.urls import *


urlpatterns = [
    url(r'^access_token/$', AccessToken.as_view(), name='access_token'),
    url(r'^auth/success/$', SuccessAuthorisation.as_view(), name='success_auth'),

]

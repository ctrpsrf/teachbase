import requests, logging

logging.basicConfig(filename="api_wrapper.log", level=logging.INFO)

class TeachBaseClient(object):

    OAUTH_URL = 'https://go.teachbase.ru/oauth/token'
    BASE_URL = 'https://go.teachbase.ru/endpoint/v1/'

    METHODS = {
        'create_user': {
            'URL': 'users/create',
            'http_methods': ['POST',],
            'context': {
                'POST': {},
                }
            },
        'lable_user': {
            'URL': 'users/labels',
            'http_methods': ['GET', 'POST', 'DELETE'],
            'context': {
                'GET': {},
                'POST': {},
                'DELETE': {},
                }
            },
        'courses': {
            'URL': 'courses',
            'http_methods': ['GET', 'POST'],
            'context': {
                'GET': {},
                'POST': {},
                }
            },
        'course': {
            'URL': 'courses/{}',
            'http_methods': ['GET', 'POST'],
            'context': {
                'GET': {},
                'POST': {},
                }
            },
        'course_sessions': {
            'URL': '/courses/{}/course_sessions',
            'http_methods': ['GET', 'POST'],
            'context': {
                'GET': {},
                'POST': {},
                }
            },
        'reg_user_to_course_session': {
            'URL': '/course_sessions/{}/register',
            'http_methods': ['POST',],
            'context': {
                'GET': {},
                }
            },
    }

    def __auth(self):
        APP_ID = '8bdf8070ca5eb1ee7565aa4722e9772a60612310f62f0a04ba4774e7527c836b'
        SECRET = 'c2c76197cc8de37d0d04a9cc4127ef7bb5c0961d4f96eeec6fff403e30b304dd'

        req = requests.post(
            self.OAUTH_URL,
            data = {'grant_type': 'client_credentials'},
            auth = (APP_ID, SECRET)
        )
        res = req.json()

        self.access_token = res['access_token']
        self.token_type = res['token_type']
        self.expires_in = res['expires_in']
        self.created_at = res['created_at']

    def __get_token(self):
        try:
            token = self.acces_token
        except AttributeError:
            self.__auth()
            token = self.access_token
        return token

    def __generate_auth_headers(self):
        token = self.__get_token()

        headers = {
            'authorization': 'Bearer {}'.format(token),
            'Content-Type': 'application/json',
        }

        return headers

    def __request(self, post_url, method, data = None):
        try:
            if method == 'POST':
                req = requests.post(
                    self.BASE_URL + post_url,
                    headers = self.__generate_auth_headers(),
                    json = data
                )
            elif method == 'GET':
                req = requests.get(
                    self.BASE_URL + post_url,
                    headers = self.__generate_auth_headers(),
                )

            res = req.json()
            if 'error' in res:
                logging.debug('{}'.format(res['error']))#, res['error_description']))
                return False
            else:
                return res
        except Exception as e:
            logging.error('System error: {}'.format(e))
            return False


    def create_user(self, email, u_id):
        data = {
          "users": [
            {
              "email": email,
              "name": None,
              "last_name": None,
              "phone": None,
              "role_id": 1,
              "auth_type": 0,
              "external_id": str(u_id)
            }
          ],
          "options": {
            "activate": False,
            "verify_emails": False,
            "skip_notify_new_users": True,
            "skip_notify_active_users": True
          }
        }

        return self.__request(
            self.METHODS['create_user']['URL'],
            data = data,
            method = 'POST')

    def get_courses(self):
        return self.__request(
            self.METHODS['courses']['URL'],
            method = 'GET')

    def get_course_by_id(self, course_id):
        return self.__request(
            self.METHODS['course']['URL'].format(str(course_id)),
            method = 'GET')

    def get_course_sessions(self, course_id):
        return self.__request(
            self.METHODS['course_sessions']['URL'].format(str(course_id)),
            method = 'GET')

    def create_course_sessions(self, course_id, data = None):
        if not data:
            data = {
                "name": "New Name",
                "started_at": "2017-06-27T19:56:09.482+03:00",
                "finished_at": "2017-06-27T19:56:09.482+03:00",
                "period": 10,
                "navigation": 0,
                "infinitely": 1,
                "access_type": 1,
                "deadline_type": 1,
                "participants": [
                    {
                      "id": 6,
                      "role_id": 1
                    }
                ]
            }

        return self.__request(
            self.METHODS['course_sessions']['URL'].format(str(course_id)),
            data = data,
            method = 'POST')

    def register_user_course_session(self, session_id, data = None):
        if not data:
            data = {
                "email": "email_1_2@factory.tb",
                "phone": 792177788666,
                "user_id": 334
            }

        return self.__request(
            self.METHODS['reg_user_to_course_session']['URL'].format(str(session_id)),
            data = data,
            method = 'POST')

    def __init__(self):
        super(TeachBaseClient, self).__init__()

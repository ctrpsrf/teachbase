from .views import APITeachbase
from django.urls import path


urlpatterns = [
    path('user/', APITeachbase.as_view(method='get_user')),
]

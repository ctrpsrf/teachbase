from django.contrib import admin
from .models import Client, AccessTokenModel


class AccessTokenAdmin(admin.ModelAdmin):
    list_display = ('user', 'client', 'token', 'expires')
    raw_id_fields = ('user',)


class ClientAdmin(admin.ModelAdmin):
    list_display = ('url', 'redirect_uri', 'client_id')

admin.site.register(AccessTokenModel, AccessTokenAdmin)
admin.site.register(Client, ClientAdmin)
